# データベース・テーブルの定義をダンプ・リストアする手順
# https://qiita.com/PlanetMeron/items/3a41e14607a65bc9b60c

# DBダンプ（バックアップ作成）
mysqldump -u USER_NAME -p -h HOST_NAME DB_NAME -d > OUTPUT_FILE_NAME

# 例
mysqldump -u root -p -h localhost kakeibo -d > dbduump.ddl


# DBリストア（復元）
mysql -u USER_NAME -p -h HOST_NAME DB_NAME < OUTPUT_FILE_NAME

# 例
mysql -u root -p -h localhost kakeibo < dbdump.ddl 
