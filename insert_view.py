import MySQLdb
import configparser
import pandas as pd
from datetime import datetime
import glob
import os

## 0.設定ファイル読み込み
config = configparser.ConfigParser()
config.read('setting.ini')

### MySQL接続情報
section1 = 'mysql'
pwd_mysql = config.get(section1, 'PASSWD')
usr_mysql = config.get(section1, 'USER')

## 1.CSVファイル読み込み
# https://teratail.com/questions/73763

DATA_DIR='./data/view_card'

files = glob.glob(os.path.join(DATA_DIR,'*.csv'))
df_list = []

# DATA_DIRディレクトリ配下の全csvファイルを読み込み
for file in files:
    tmp_df = pd.read_csv(file, encoding="SHIFT-JIS",skiprows=7, header=None ,usecols=[0,1,2] , engine='python')
    df_list.append(tmp_df)
df = pd.concat(df_list, ignore_index=True)

#カラム名を変更
df_new = df.rename(columns={0:'date', 1:'summary', 2:'payment'})
#YYYY年MM月DD日->YYYY-MM-DDへ変更
for index, row in df_new.iterrows():
    #date型へ変更
    bd = datetime.strptime(row[0], "%Y/%m/%d")
    #文字列へ変更
    row[0] = bd.strftime("%Y-%m-%d")
    #実データの値を変更
    df_new.at[index,'date'] = row[0]
    #カンマの除去
    df_new.at[index,'payment'] = int(str(row[2]).replace(",", ""))

# dateカラムでソート
df_s = df_new.sort_values('date')

## 2.DB接続
conn = MySQLdb.connect(db='kakeibo', user=usr_mysql, passwd=pwd_mysql, charset='utf8')
c = conn.cursor()

## 3.データ登録
insert_cash = 'insert into view_card (date, summary, payment) values (%s, %s, %s);'
for index, row in df_s.iterrows():
   c.execute(insert_cash, row) 

conn.commit()
conn.close()
