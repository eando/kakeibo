import MySQLdb
import configparser
import pandas as pd
from datetime import datetime

## 0.設定ファイル読み込み
config = configparser.ConfigParser()
config.read('setting.ini')

### MySQL接続情報
section1 = 'mysql'
pwd_mysql = config.get(section1, 'PASSWD')
usr_mysql = config.get(section1, 'USER')

## 1.ファイル読み込み

# 列名を変更してcsv読み込み
# https://qiita.com/daifuku_mochi2/items/9a82d381440ab073a060
epos = pd.read_csv("data/mufj_bank/3244219_20180911153141.csv", encoding="SHIFT-JIS", usecols=[0,1,2,3,4,5,8], engine='python')

#カラム名を変更
epos_new = epos.rename(columns={'日付': 'date','摘要': 'summary','摘要内容': 'summary_content','支払い金額': 'payment','預かり金額': 'deposit','差引残高': 'account_balance','入払区分': 'accounts'})

# 欠損値NaNを置換
epos_new2 = epos_new.fillna({'summary_content': '', 'payment':'0', 'deposit': '0'})

#YYYY/MM/DD->YYYY-MM-DDへ変更
for index, row in epos_new2.iterrows():
    #date型へ変更
    bd = datetime.strptime(row[0], "%Y/%m/%d")
    #文字列へ変更
    row[0] = bd.strftime("%Y-%m-%d")
    #実データの値を変更
    epos_new2.at[index,'date'] = row[0]

    #カンマの除去
    epos_new2.at[index,'payment'] = int(str(row[3]).replace(",", ""))
    epos_new2.at[index,'deposit'] = int(str(row[4]).replace(",", ""))
    epos_new2.at[index,'account_balance'] = int(str(row[5]).replace(",", ""))

## 2.DB接続
conn = MySQLdb.connect(db='kakeibo', user=usr_mysql, passwd=pwd_mysql, charset='utf8')
c = conn.cursor()

## 3.データ登録
insert_cash = 'insert into mufj_bank (date, summary, summary_content, payment, deposit, account_balance, accounts) values (%s, %s, %s, %s, %s, %s, %s);'
for index, row in epos_new2.iterrows():
   c.execute(insert_cash, row) 

conn.commit()
conn.close()
