import MySQLdb
import configparser
import pandas as pd
from datetime import datetime

## 0.設定ファイル読み込み
config = configparser.ConfigParser()
config.read('setting.ini')

### MySQL接続情報
section1 = 'mysql'
pwd_mysql = config.get(section1, 'PASSWD')
usr_mysql = config.get(section1, 'USER')

## 1.ファイル読み込み

# 列名を変更してcsv読み込み
# https://qiita.com/daifuku_mochi2/items/9a82d381440ab073a060
epos = pd.read_csv("data/epos_card/20180909_UseHistoryReference.csv", encoding="SHIFT-JIS", skiprows=1,  skipfooter=4, usecols=[1,2,3,4,6], engine='python')

#カラム名を変更
epos_new = epos.rename(columns={'ご利用年月日': 'buydate', 'ご利用場所':'shop', 'ご利用内容':'item','ご利用金額（キャッシングでは元金になります）':'cashout', 'お支払開始月':'paymentmonth'})

#YYYY年MM月DD日->YYYY-MM-DDへ変更
for index, row in epos_new.iterrows():
    #date型へ変更
    bd = datetime.strptime(row[0], "%Y年%m月%d日")
    pm = datetime.strptime(row[4], "%Y年%m月")
    #文字列へ変更
    row[0] = bd.strftime("%Y-%m-%d")
    row[4] = pm.strftime("%Y%m")
    #実データの値を変更
    epos_new.at[index,'buydate'] = row[0]
    epos_new.at[index,'paymentmonth'] = row[4]

    #文字コード変換SJIS->UTF8
    row[1].encode('utf-8')
    row[2].encode('utf-8')
    epos_new.at[index,'shop'] = row[1]
    epos_new.at[index,'item'] = row[2]

## 2.DB接続
conn = MySQLdb.connect(db='kakeibo', user=usr_mysql, passwd=pwd_mysql, charset='utf8')
c = conn.cursor()

## 3.データ登録
insert_cash = 'insert into epos_card (buydate,shop,item,cashout,paymentmonth) values (%s, %s, %s, %s, %s);'
for index, row in epos_new.iterrows():
   c.execute(insert_cash, row) 

conn.commit()
conn.close()
